extends Control
@onready var grid = $VBoxContainer/GridContainer
var buttons = preload("res://item.tscn")

var items = []

# Called when the node enters the scene tree for the first time.
func _ready():
	Global.bingo = false
	randomize()
	# limit number of signs
	Items.signs.shuffle()
	Items.signs.resize(5)
	
	items.append_array(Items.base)
	items.append_array(Items.signs)
	items.append_array(Items.animals)
	items.append_array(Items.vehicles)
	items.shuffle()
	load_items()

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	pass

func load_items():
	for i in range(0,25):
		var button = buttons.instantiate()
		if i == 12:
			button.set_text("FREE")
			button.button_pressed = true
		else:
			button.icon = ResourceLoader.load("res://" + items[i])
		grid.add_child(button)


func _on_reset_pressed():
	$ConfirmationDialog.show()


func _on_confirmation_dialog_confirmed():
	get_tree().reload_current_scene()


func _on_exit_pressed():
	$quitDialog.show()

func _on_quit_dialog_confirmed():
	get_tree().quit()
