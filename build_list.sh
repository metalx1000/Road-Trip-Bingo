#!/bin/bash

output="src/scripts/item_list.gd"
echo -e "extends Node\n" >"$output"

ls -d src/items/*/ | while read folder; do
	list="$(basename "$folder")"
	echo "var ${list} = ["
	ls $folder/*.png | while read item; do
		echo -e "\t'$folder$(basename $item)'," | sed 's|src/||g'
	done
	echo -e "]\n"
done >>"$output"
