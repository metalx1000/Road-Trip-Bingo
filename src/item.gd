extends Button


var groups = []

func bingo():

	if Global.bingo:
		return
		
	Global.bingo = true

	$victory_sound.play()

func check_bingo(a,b,c,d,e):
	var items_g = get_tree().get_nodes_in_group("items")
	# Check rows
	if items_g[a].button_pressed &&\
		items_g[b].button_pressed &&\
		items_g[c].button_pressed &&\
		items_g[d].button_pressed &&\
		items_g[e].button_pressed:
			return true
	
func _on_pressed():
	var items_g = get_tree().get_nodes_in_group("items")

# Check rows
	if check_bingo(0,1,2,3,4):
		bingo()
	elif check_bingo(5,6,7,8,9):
		bingo()
	elif check_bingo(10,11,12,13,14):
		bingo()
	elif check_bingo(15,16,17,18,19):
		bingo()
	elif check_bingo(20,21,22,23,24):
		bingo()
	
# Check columns
	if check_bingo(0,5,10,15,20):
		bingo()
	elif check_bingo(1,6,11,16,21):
		bingo()
	elif check_bingo(2,7,12,17,22):
		bingo()
	elif check_bingo(3,8,13,18,23):
		bingo()
	elif check_bingo(4,9,14,19,24):
		bingo()
	elif check_bingo(0,6,12,18,24):
		bingo()
	elif check_bingo(4,8,12,16,20):
		bingo()
	else:
		Global.bingo = false
