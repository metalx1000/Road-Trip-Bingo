
# Road-Trip-Bingo

Copyright Kris Occhipinti 2024-05-13

(https://filmsbykris.com)

License GPLv3

# Credits
Some Icons are from - https://emoji.aranja.com/
Victory sound - Sound Effect from <a href="https://pixabay.com/sound-effects/?utm_source=link-attribution&utm_medium=referral&utm_campaign=music&utm_content=6185">Pixabay</a>
<a href="https://www.freepik.com/free-vector/recycling-truck-isolated-white-background-vector-illustration_16312489.htm#query=cartoon%20garbage%20truck&position=4&from_view=keyword&track=ais_user&uuid=26b34a50-69df-4fcf-9473-53c811703cf4">Image by callmetak</a> on Freepik
<a href="https://www.freepik.com/free-vector/two-cars-tractor_1359955.htm#fromView=search&page=1&position=3&uuid=c3d789fd-9a85-48c0-b14f-6b3f05cfa9dd">Image by freepik</a>
